<?php

include('classes/conexao.php');
include('classes/header.php');
include('classes/funcoes.php');

$action=isset($_REQUEST['cod_task'])?'alter_task.php?cod_task='.$_REQUEST['cod_task']:'insert_task.php';

$cod_task=isset($_REQUEST['cod_task'])?$_REQUEST['cod_task']:'';

$titulo = '';
$descricao = '';
$file = '<input type="file" name="arquivo" />';


if($cod_task != ''){
  $select ="select * from task where cod_task='$cod_task'";

  $bd = mysqli_query($conexao, $select) or die($select);

  while ($dados = mysqli_fetch_array($bd)) {
    $titulo = $dados['titulo_task'];
    $descricao = $dados['descricao_task'];
    $file = '<a href=arquivos/'.utf8_encode($dados['arquivo_task']).' target=_blank> arquivos/'.$dados['arquivo_task']."</a><br><br>";
    }

}
 ?>

 <div class='navbar navbar-inverse' style='border-radius:0; width: 100%'>

   <div class="navbar-header">
      <a class="navbar-brand" href="principal.php">Task Manager</a>
   </div>

   <a href='#' class='btn btn-danger btn-xs sair' onclick="sair()"><span class='glyphicon glyphicon-remove'></a>

 </div>

<div class='div-form'>
    <h3>Cadastro de Task:</h3>


    <form onsubmit="return valida(this)" action='<?php echo $action; ?>' method='post' enctype="multipart/form-data">

        <label for="cod_task">Cod. Task:</label>
        <input type="tel" class="form-control" id="cod_task" placeholder="Cod. Task" name="cod_task" value='<?php echo $cod_task ?>' disabled>

        <label for="cod_task">Nome Task:</label>
        <input type="text" class="form-control" id="nome_task" placeholder="Nome Task" name="nome_task" value='<?php echo $titulo ?>'>

        <label for="cod_task">Descrição da Task:</label>
        <textarea class="form-control" id="desc_task" placeholder="Nome Task" name="desc_task" rows="6"><?php echo $descricao ?></textarea>
        <br />
        <?php echo $file ?>

        <input type='submit' class='btn btn-primary btn-xm' value='Gravar'>
        <a href='principal.php' class='btn btn-danger brn-xm'>Cancelar</a>
    </form>


</div>


<div class='div-result'>

<table class="table table-striped" align=center>

  <thead>
  <tr>
    <th align='center'><center><b>#</b></center></th>
    <th align='center'><center><b>Título</b></center></th>
    <th align='center'><center><b>Descição</b></center></th>
    <th align='center'><center><b>Ações</b></center></th>
  </tr>
</thead>

<?php
  $select = 'select * from task where excluido=0';

  $bd = mysqli_query($conexao, $select) or die ($select);

  while ($dados = mysqli_fetch_array($bd)) {
    ?>
    <tr>
      <td><?php echo $dados['cod_task']; ?> </td>
      <td><?php echo $dados['titulo_task']; ?> </td>
      <td><?php echo $dados['descricao_task']; ?> </td>
      <td>
        <a href="principal.php?cod_task=<?php echo $dados['cod_task']; ?>" class='btn btn-primary btn-sm' alt='Alterar' title='Alterar'><span class='glyphicon glyphicon-pencil'></a>

          <script>
          function deleta<?php echo $dados['cod_task'];?>() {
              var r = confirm("Deseja Realmente excluir a tarefa?");
              if (r == true) {
                  location.href="deleta_task.php?cod_task=<?php echo $dados['cod_task']; ?>"
              }
            }
          </script>

        <a href='#' onclick=deleta<?php echo $dados['cod_task'];?>() class='btn btn-danger btn-sm' title='Excluir'><span class='glyphicon glyphicon-remove'></span></a></td>





       </td>
    </tr>


    <?php

  }



 ?>






</div>
