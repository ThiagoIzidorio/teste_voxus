-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           5.7.14 - MySQL Community Server (GPL)
-- OS do Servidor:               Win64
-- HeidiSQL Versão:              9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Copiando estrutura do banco de dados para task
CREATE DATABASE IF NOT EXISTS `task` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `task`;

-- Copiando estrutura para tabela task.task
CREATE TABLE IF NOT EXISTS `task` (
  `cod_task` int(11) NOT NULL AUTO_INCREMENT,
  `titulo_task` varchar(50) DEFAULT NULL,
  `descricao_task` text,
  `arquivo_task` varchar(255) DEFAULT NULL,
  `excluido` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`cod_task`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela task.task: 1 rows
/*!40000 ALTER TABLE `task` DISABLE KEYS */;
INSERT INTO `task` (`cod_task`, `titulo_task`, `descricao_task`, `arquivo_task`, `excluido`) VALUES
	(7, 'Teste de Nome 4', 'teste de upload uploada\r\n\r\nupload', 'IMG_9820.jpg', 1);
/*!40000 ALTER TABLE `task` ENABLE KEYS */;

-- Copiando estrutura para tabela task.user
CREATE TABLE IF NOT EXISTS `user` (
  `email` varchar(50) DEFAULT NULL,
  `senha` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela task.user: 1 rows
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`email`, `senha`) VALUES
	('thiagoizidorio@voxus.tv', '123456');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
